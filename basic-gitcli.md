# Git and GitLab Workshop

<img src="image/git-scm.png" width="300" >

Please download https://git-scm.com/downloads and install.

## Prepare Git Repository

* Config git user and email for commit user data first

## Contents
- 1.1 Git config global
- 1.2 Git commit 
- 1.3 Push to remote repository 
- 1.4 Create your subgroup 
- 1.5 Create first project 
- 1.6 Create branch 
- 1.7 Create merge request 

## 1.1 Open Gitbash

```bash
git config --global user.name "your-name"
git config --global user.email "your-name@your-domain-company"

# Example config 
git config --global user.name "firstname  lastname"
git config --global user.email "firstname@got.co.th"

# See your git config
git config --list
```



## 1.2 First Git Commit

* Put command `git status` to see repository status
* Put commands below for first commit

```bash
git add 'new files'
git status
git commit -m "Initial commit"
git status
```

* Put command `git log` or `git log --oneline` to see history of commit 

</br>

## 1.3  Push Repository to GitLab
1. สร้าง SSH key pair จากในเครื่อง client โดยเลือกประเภทจากด้านล่าง
    - 1.1 ED25519 

    - 1.2 RSA

2. เลือกใช้ command ตามด้านล่าง และใส่ข้อมูลให้เรียบร้อย
  #### For example, for ED25519:

``` 
ssh-keygen -t ed25519 -C "<comment>" 
```

  #### For 2048-bit RSA:

```
ssh-keygen -t rsa -b 2048 -C "<comment>"
```

```bash 
cat ~/.ssh/id_rsa.pub
# Copy your public key  
```
### Add your SSH Public Key to GitLab

* Go to <https://gitlab.com> and login with your credential
* Go to <https://gitlab.com/-/profile/keys> or menu `Settings` on your avatar icon on the top right and choose menu `SSH Keys` on the left
* Put your public key on the `Key` textbox and click `Add key`

## 1.4 Create your own group and subgroup

* Go to `Groups` > `Your group` menu on the top left
* Click on `Your group` group
* Click on `New subgroup` on the top right then fill your information and ckick `Create subgroup`

### Manage member permission 
- At the group or project level
- Go to manage menu > member
- Choose a role for your member

## 1.5 Create your first project

* Go to group level

<img src="image/basic-git-1.png" width="200" style="border: 2px solid black;">

* Click at your main group

<img src="image/basic-git-2.png" style="border: 2px solid black;">

* Click on your newly created subgroup `[your-name]`

<img src="image/basic-git-3.png" style="border: 2px solid black;">

* Fill an information and create subgroup

<img src="image/basic-git-4.png" style="border: 2px solid black;">

* Click on New project

<img src="image/basic-git-5.png" style="border: 2px solid black;">

* Create your blank project

<img src="image/basic-git-6.png" style="border: 2px solid black;">

  * Eg. Project name: nginx

<img src="image/basic-git-7.png" style="border: 2px solid black;">

* Project created 

<img src="image/basic-git-8.png" style="border: 2px solid black;">

### Add remote repository and push code

* Copy `git remote add origin` command in `Push an existing folder` section
* Push code to GitLab On Cloud Shell

```bash
git remote add origin git@gitlab.com:[your-group]/[your-name]/nginx.git

# To see remote repository has been added
git remote -v
git push -u origin main
# Maybe you need to answer yes for the first time push
```

* Refresh nginx main page on GitLab again to see change



## 1.6 Create dev branch for develop

* Put these commands to create dev branch

```bash
git branch
git branch dev
git branch
git checkout dev
git branch
git push origin dev
```

## .gitignore file

* Create new file name `.gitignore` and push these content

```gitignore
.project
.settings
```

## 1.7 Merge Requests

* Go to menu `Merge Requests` on GitLab
* Click on `Create merge request`
* See `Commits` and `Changes` tabs. You can leave everything default and click on `Submit merge request`
* Since you are maintainer, you can click on `Merge` button to merge code from `dev` to `main` branch.
* See your changes on main branch


Finally files directory will be shown below structure, then please commit and push to your remote repo.

```
.
├── .gitignore
└── src
    ├── hello.conf
    └── index.html
```
